﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APITEST.CONCUR_API;

namespace APITEST
{
    class Program
    {
        static void Main(string[] args)
        {
            Acumatica ACM = new Acumatica();
            string token;
            List<Expense> expenseList = new List<Expense>();

            token = ACM.getConcurToken();
            expenseList = ACM.getConcurExpense(token);
            ACM.pushToACM(expenseList);
        }
    }
}
