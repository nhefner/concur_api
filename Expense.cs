﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APITEST
{
    public class Expense
    {
        public string id;
        public string ID
        {
            get { return id; }
            set { id = value; } 
        }
        public string uri;
        public string Uri
        {
            get { return uri; }
            set { uri = value; }
        }
        public string reportid;
        public string Reportid
        {
            get { return reportid; }
            set { reportid = value; }
        }
        public string reportownerid;
        public string Reportownerid
        {
            get { return reportownerid; }
            set { reportownerid = value; }
        }
        public string expenseTypeCD;
        public string ExpenseTypeCD
        {
            get { return expenseTypeCD; }
            set { expenseTypeCD = value; }
        }
        public string expenseTypeName;
        public string ExpenseTypeName
        {
            get { return expenseTypeName; }
            set { expenseTypeName = value; }
        }
        public string spendCategoryCD;
        public string SpendCategoryCD
        {
            get { return spendCategoryCD; }
            set { spendCategoryCD = value; }
        }
        public string spendCategoryName; 
        public string SpendCategoryName
        {
            get { return spendCategoryName; }
            set { spendCategoryName = value; }
        }
        public string paymentTypeid;
        public string PaymentTypeid
        {
            get { return paymentTypeid; }
            set { paymentTypeid = value; }
        }
        public string paymentTypeName;
        public string PaymentTypeName
        {
            get { return paymentTypeName; }
            set { paymentTypeName = value; }
        }
        public string transactionDate;
        public string TransactionDate
        {
            get { return transactionDate; }
            set { transactionDate = value; }
        }
        public string transactionCurrencyCD;
        public string TransactionCurrencyCD
        {
            get { return transactionCurrencyCD; }
            set { transactionCurrencyCD = value; }
        }
        public string transactionAmt;
        public string TransactionAmt
        {
            get { return transactionAmt; }
            set { transactionAmt = value; }
        }
        public string exchangeRate;
        public string ExchangeRate
        {
            get { return exchangeRate; }
            set { exchangeRate = value; }
        }
        public string postedAmt;
        public string PostedAmt
        {
            get { return postedAmt; }
            set { postedAmt = value; }
        }
        public string approvedAmt;
        public string ApprovedAmt
        {
            get { return approvedAmt; }
            set { approvedAmt = value; }
        }
        public string vendorDesc;
        public string VendorDesc
        {
            get { return vendorDesc; }
            set { vendorDesc = value; }
        }
        /* mOre vendor details here*/
        public string desc;
        public string Desc
        {
            get { return desc; }
            set { desc = value; }
        }
        public string isPersonal;
        public string IsPersonal
        {
            get { return isPersonal; }
            set { isPersonal = value; }
        }
        public string isBillable;
        public string IsBillable
        {
            get { return isBillable; }
            set { isBillable = value; }
        }
        public string isPersonalCardCharge;
        public string IsPersonalCardCharge
        {
            get { return isPersonalCardCharge; }
            set { isPersonalCardCharge = value; }
        }
        
        //public void Dispose()
        //{
        //    id = String.Empty;
        //    uri = String.Empty;
        //    reportid = String.Empty;
        //    reportownerid = String.Empty;
        //    expenseTypeCD = String.Empty;
        //    expenseTypeName = String.Empty;
        //    spendCategoryCD = String.Empty;
        //    spendCategoryName = String.Empty;
        //    paymentTypeid = String.Empty;
        //    paymentTypeName = String.Empty;
        //    transactionDate = String.Empty;
        //    transactionCurrencyCD = String.Empty;
        //    TransactionAmt = String.Empty;
        //    exchangeRate = String.Empty;
        //    postedAmt = String.Empty;
        //    approvedAmt = String.Empty;
        //    vendorDesc = String.Empty;
        //    desc = String.Empty;
        //    isPersonal = String.Empty;
        //    isBillable = String.Empty;
        //    isPersonalCardCharge = String.Empty;

        //}
    }
}
