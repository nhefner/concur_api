﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using APITEST.CONCUR_API;

namespace APITEST
{

    public class Acumatica
    {

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
                return "";
        }

        public static string getNextEntries(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(End, strSource.Length - End);
            }
            else
                return "";
        }

        public string getConcurToken()
        {
            string username = "nick.g.hefner@gmail.com";
            string password = "Klear.2009!!";
            string urllogin = @"https://www.concursolutions.com/net2/oauth2/accesstoken.ashx";
            
            string jsonResponse, token;


            string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            try
            {
                var webRequest = System.Net.WebRequest.Create(urllogin);
                if (webRequest != null)
                {

                    webRequest.Method = "GET";
                    webRequest.Timeout = 20000;
                    webRequest.ContentType = "aplication/json";
                    webRequest.Headers.Add("Authorization", "Basic " + encoded);
                    webRequest.Headers.Add("X-ConsumerKey", "7mTvadfzY1epRzKVtFPhrs");
                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            jsonResponse = sr.ReadToEnd();
                            Console.WriteLine(String.Format("Response: {0}", jsonResponse));
                            //Console.WriteLine("Press any key to continue");
                            //Console.ReadKey();
                            token = getBetween(jsonResponse, "<Token>", "</Token>");
                            Console.WriteLine(String.Format("Response: {0}", token));
                            //Console.WriteLine("Press any key to continue");
                           // Console.ReadKey();
                            return token;
                        }
                    }
                }
                else
                {
                    return "";
                }   
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Console.WriteLine("Press any key to continue");
               // Console.ReadKey();
                return "";
            }

        }



        public List<Expense> getConcurExpense(string token)
        {
            string urlGetReports = @"https://www.concursolutions.com/api/v3.0/expense/entries/";
            string jsonResponse2, entry;
            List<Expense> returnList = new List<Expense>();
            //Expense expense = new Expense();

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            try
            {
                var webRequest2 = System.Net.WebRequest.Create(urlGetReports);
                if (webRequest2 != null)
                {
                    webRequest2.Method = "GET";
                    webRequest2.Timeout = 20000;
                    webRequest2.ContentType = "aplication/json";
                    webRequest2.Headers.Add("Authorization", "OAuth " + token);
                    using (System.IO.Stream s = webRequest2.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            jsonResponse2 = sr.ReadToEnd();
                            //Console.WriteLine(String.Format("Response: {0}", jsonResponse2));
                            //Console.WriteLine("Press any key to continue");

                           // Console.ReadKey();
                            while (jsonResponse2 != "") // iterate through the expense entries
                            {
                                entry = getBetween(jsonResponse2, "<Entry>", "</Entry>"); // grab entry

                                if (entry != "")    // there are still entries (start filling in expense object)
                                {
                                    Expense expense = new Expense();
                                    
                                        expense.id = getBetween(entry, "<ID>", "</ID>");
                                        expense.desc = getBetween(entry, "<Description>", "</Description>");
                                        expense.transactionAmt = getBetween(entry, "<TransactionAmount>", "</TransactionAmount>");
                                        expense.expenseTypeName = getBetween(entry, "<ExpenseTypeName>", "</ExpenseTypeName>");
                                        expense.transactionDate = getBetween(entry, "<TransactionDate>", "</TransactionDate>");
                                        returnList.Add(expense);
                                    
                                   
                                }

                                jsonResponse2 = getNextEntries(jsonResponse2, "<Entry>", "</Entry>");
                               // Console.WriteLine(String.Format("Response: {0}", jsonResponse2));
                                //Console.WriteLine("Press any key to continue");
                                //Console.ReadKey();

                            }
                        }
                    }

                }
                return returnList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Console.WriteLine("Press any key to continue");
                //Console.ReadKey();
                return returnList;

            }
        }

        public void getConcurPaidPayments()
        {
            string urlGetPayments = @"https://www.concursolutions.com/api/v3.0/expense/reports";
            string jsonResponse3;
            List<string> returnList = new List<string>();

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls12;
            try
            {

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public void pushToACM(List<Expense> expenseList)
        {
            CONCUR_API.Screen context = new CONCUR_API.Screen();     //Declare screen object based on web service

            context.CookieContainer = new System.Net.CookieContainer();   //Begin context setup
            context.AllowAutoRedirect = true;
            context.EnableDecompression = true;
            context.Timeout = 1000000;

            context.Url = "http://localhost/SmartyPants2/(W(11))/Soap/APITEST.asmx";   //grab the specific screen.

            LoginResult result = context.Login("klear", "Klear.2009!!");               //Acumatica login

            /*--------------------------------------------------------*/


            try
            {
                string desc = "Expense entries as of " + DateTime.Now.ToString();                   //desc + date for data being pushed in
                AP301000Content AP301000 = context.AP301000GetSchema();                             //grab schema of the Bills and adjustments page
                context.AP301000Clear();

                var cmds = new List<Command>();                                                     // create a new list of commands to map into Acumatica
                cmds.Add(
                    new Value { Value = "Bill", LinkedCommand = AP301000.DocumentSummary.Type }     // Add type
                    );
                cmds.Add(
                    new Value { Value = "0000", LinkedCommand = AP301000.DocumentSummary.ReferenceNbr } // Add RefNbr
                    );
                cmds.Add(
                     new Value { Value = "AMEX", LinkedCommand = AP301000.DocumentSummary.Vendor }      // Add Vendor
                    );
                cmds.Add(
                    new Value { Value = desc, LinkedCommand = AP301000.DocumentSummary.Description}     // Add Description
                    );

                foreach(Expense expense in expenseList)
                {
                    cmds.Add(
                        AP301000.DocumentDetails.ServiceCommands.NewRow);
                    cmds.Add(
                        new Value { Value = expense.expenseTypeName + "--" + expense.desc, LinkedCommand = AP301000.DocumentDetails.TransactionDescr }
                        );
                    cmds.Add(
                        new Value { Value = "1", LinkedCommand = AP301000.DocumentDetails.Quantity }
                        );
                    cmds.Add(
                         new Value { Value = expense.transactionAmt, LinkedCommand = AP301000.DocumentDetails.UnitCost }
                         );
                    cmds.Add(
                        new Value { Value = expense.transactionDate, LinkedCommand = AP301000.DocumentDetails.ExpenseDate });
                }

                cmds.Add(AP301000.Actions.Save);
                
                    AP301000Content[] AP301000result = context.AP301000Submit(cmds.ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }    
        }
    }
}
